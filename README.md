# 通用模块

自用模块

## 安装教程

`pip install 模块名`

## 使用说明

- color_log

```python
from color_log import Logger

log = Logger()
log.info(msg)
log.error(msg)
```

inux系统使用 `tail -F -s5 logs/log` 实时监控日志变化

- smtp_send

```python
from smtp_send import smtp_send

smtp_send('smtp服务器:端口', ['测试员', 'wujh@ebfcn.com.cn'], '密码', ['收件人1','收件人2'], ['抄送人1','抄送人2'], '标题', '内容', ['附件1','附件2'])
```
